# Управление учетными записями
Проект веб-приложения для управления учетными записями к информационным системам. Предусмотрено 3 кабинета - для пользователей ИС, вендоров и администраторов.

Некоторые скриншоты рабочей области
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/1.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/2.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/22.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/3.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/31.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/32.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/4.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/5.png)
![screen](https://gitlab.com/pavloid21/accounts/raw/master/accounts/6.png)
