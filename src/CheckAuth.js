import React, { Component } from "react";
import Login from "./components/Login";
import MoChooser from "./components/MoChooser";
import { RiseLoader } from "halogenium";

let flag = false;
var axios = require("axios");

export default class CheckAuth extends Component {
  constructor(props) {
    super();
    console.log("This happens 1st.");
    this.state = {
      loading: false,
      data: ""
    };
  }

  loadData() {
    var promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("This happens 5th (after 3 seconds). " + this.props.token);

        resolve("");
      }, 3000);
    });
    console.log("This happens 3rd.");
    axios
      .get("http://localhost:3000/api/systems/", {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.props.token
        }
      })
      .then(function() {
        flag = true;
        console.log("then");
      })
      .catch(function() {
        flag = false;
        console.log("catch");
      });
    return promise;
  }

  componentWillMount(props) {
    console.log("This happens 2nd.");

    //const cookies = new Cookies();
    this.setState({ loading: true });
    this.loadData().then(data => {
      console.log("This happens 6th.");
      this.setState({
        data: data,
        loading: false
      });
    });
  }

  render() {
    if (this.state.loading) {
      console.log("This happens 4th - when waiting for data.");
      return (
        <div className="hero is-fullheight">
          <div className="hero-body">
            <div className="container has-text-centered">
              <RiseLoader color="#00d1b2" />
            </div>
          </div>
        </div>
      );
    }
    console.log(flag);
    console.log("This happens 7th - after I get data.");
    if (flag) {
      return <MoChooser token={this.props.token} />;
    } else {
      return <Login />;
    }
  }
}
