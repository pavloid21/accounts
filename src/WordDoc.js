import React from 'react';
import {render, Document, Text} from 'redocx';

export default class WordDoc extends React.Component {
  render(){
    return(
      <Document>
        <Text>Hello</Text>
      </Document>
    )
  }
}

render(<WordDoc/>, '${__dirname}/example.docx')
