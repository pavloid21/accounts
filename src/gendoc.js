import {JSZipUtils} from 'jszip';
var DocxGen = require('docxtemplater');
var LinkModule = require('docxtemplater-link-module');
var linkModule = new LinkModule();
var fs = require('file-saver');


export function loadFile(url,callback){
    JSZipUtils.getBinaryContent(url,callback);
}
loadFile("./template/template.docx",function(error,content){
    if (error) { throw error };
    //var zip = new JSZip(content);
    var doc=new DocxGen().attachModule(linkModule).load(content).setData({
                "clients":[{
                  "first_name":"John",
                  "last_name":"Doe",
                  "phone":"+44546546454"
                },{
                  "first_name":"Jane",
                  "last_name":"Doe",
                  "phone":"+445476454"
                }]});

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        doc.render()
    }
    catch (error) {
        var e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
        }
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
        throw error;
    }

    var out=doc.getZip().generate({
        type:"blob"
    }) //Output the document using Data-URI
    //saveAs(out,"output.docx")
    fs.writeFileSync("output.docx", out);
})
