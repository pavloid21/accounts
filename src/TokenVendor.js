import React, {Component} from 'react';
import CheckAuthVendor from './CheckAuthVendor';
import Cookies from 'universal-cookie';

class Token extends Component {
  render(){
    const cookies = new Cookies();
    return(
      <CheckAuthVendor token={cookies.get('token')}/>
    )
  }
}
export default Token;
