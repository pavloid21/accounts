import React, {Component} from 'react';
import CheckAuth from './CheckAuth';
import Cookies from 'universal-cookie';

class Token extends Component {
  render(){
    const cookies = new Cookies();
    return(
      <CheckAuth token={cookies.get('token')}/>
    )
  }
}
export default Token;
