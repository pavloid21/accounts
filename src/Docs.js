import React, { Component } from "react";

class Docs extends Component {
  render() {
    return (
      <div className="container">
        <div className="content">
          <p className="title">Документы и инструкции</p>
          <div class="box">
            <article class="media">
              <div class="media-left">
                <figure class="image is-64x64">
                  <img
                    src="https://www.file-extensions.org/imgs/app-icon/128/1196/microsoft-word-icon.png"
                    alt="Avatar"
                  />
                </figure>
              </div>
              <div class="media-content">
                <div class="content">
                  <p>
                    <strong>
                      Порядок получения (замены) учётных записей пользователей
                      региональных информационных систем
                    </strong>
                    <br />
                    Инструкция и порядок получения (замены) учётных записей
                    пользователей региональных информационных систем, форма
                    заявления.
                  </p>
                </div>
                <a
                  href="http://medportal.saratov.gov.ru/media/cms_page_media/4393/poryadok-polucheniya-uchetnyih-zapisej-loginov-parolej20.doc"
                  className="button is-link"
                >
                  Скачать
                </a>
              </div>
            </article>
          </div>
        </div>
      </div>
    );
  }
}
export default Docs;
