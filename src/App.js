import React, { Component } from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Main from "./components/Main";
import Info from "./Info";
import Docs from "./Docs";
import Nav from "./components/Nav";
import MoCabinet from "./components/MoCabinet";
import Temp from "./Temp";
import Token from "./Token";
import TokenVendor from "./TokenVendor";
import SystemChooser from "./components/SystemChooser";
import RequestsList from "./components/RequestsList";
import StatusChooser from "./components/StatusChooser";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/info" component={Info} />
          <Route exact path="/docs" component={Docs} />
          <Route exact path="/mo-cabinet" component={MoCabinet} />
          <Route exact path="/mo-cabinet/send-request" component={Temp} />
          <Route exact path="/login" component={Token} />
          <Route exact path="/sys-chooser" component={SystemChooser} />
          <Route exact path="/requests-list" component={RequestsList} />
          <Route exact path="/type-chooser" component={StatusChooser} />
          <Route exact path="/login-vendor" component={TokenVendor} />
        </Switch>
      </div>
    );
  }
}

export default App;
