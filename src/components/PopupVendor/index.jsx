import React, { Component } from "react";
import Axios from "axios";
import Cookies from "universal-cookie";
import "../../css/popup.css";

class DetailsPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],
      logins:[],
      filename: ""
    };
  }

  componentWillMount() {
    let cookies = new Cookies();
    let id = this.props.title;
    console.log("title", this.props.typereq);
    Axios.get("http://localhost:3000/api/request_details/" + id, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        Authorization: "Bearer " + cookies.get("token")
      }
    }).then(response => {
      var details = response.data;
      return details.map(item => {
        this.setState({ details: details });
      });
    });
  }

  handleSave() {

  }

  loginChanged(e) {
    const item = {
      name: e.target.name,
      value: e.target.value,
      id: e.target.id
    }
    console.log(item);
    const details = this.state.details.slice();
    const newDetails = details.map((detail) => {
      for (var key in detail) {
        console.log(detail.id === item.id);
        if (key === item.name && detail.id.toString() === item.id) {
          detail[key] = item.value
        }
      }
      return detail;
    });
    this.setState({ details: newDetails });
    console.log(this.state.details);
  }

  passChanged(e) {
      const item = {
        name: e.target.name,
        value: e.target.value,
        id: e.target.id
      }
      console.log(item);
      const details = this.state.details.slice();
      const newDetails = details.map((detail) => {
        for (var key in detail) {
          console.log(detail.id === item.id);
          if (key === item.name && detail.id.toString() === item.id) {
            detail[key] = item.value
          }
        }
        return detail;
      });
      this.setState({ details: newDetails });
      console.log(this.state.details);
  }

  render() {
    if (!this.props.modalState) {
      return null;
    }
    return (
      <div className="modal is-active">
        <div className="modal-background" onClick={this.props.closeModal} />
        <div className="modal-card" style={{width:'800px'}}>
          <header className="modal-card-head">
            <p className="modal-card-title">
              Список из заявки # {this.props.title}
            </p>
            <button className="delete" onClick={this.props.closeModal} />
          </header>
          <section className="modal-card-body">
            <div className="content is-size-7">
              <table className="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Должность</th>
                    <th>Дата рождения</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th>Логин</th>
                    <th>Пароль</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.details.map(item => {
                    var date = new Date(item.birth_date);
                    return (
                      <tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.position}</td>
                        <td>
                          {date.getFullYear()}-{date.getMonth() + 1}-{date.getDate()}
                        </td>
                        <td>{item.surname}</td>
                        <td>{item.name}</td>
                        <td>{item.father_name}</td>
                        <td>
                          <input
                            id={item.id}
                            name="login"
                            className="input"
                            type="text"
                            value={item.login}
                            onChange={this.loginChanged.bind(this)}
                          />
                        </td>
                        <td>
                          <input
                            id={item.id}
                            name="password"
                            className="input"
                            type="text"
                            value={item.password}
                            onChange={this.passChanged.bind(this)}
                          />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </section>
          <footer className="modal-card-foot">
            <a className="button" onClick={this.props.closeModal}>
              Закрыть
            </a>
            <button className="button is-primary" onClick={this.handleSave}>Сохранить</button>
          </footer>
        </div>
      </div>
    );
  }
}

export default DetailsPopup;
