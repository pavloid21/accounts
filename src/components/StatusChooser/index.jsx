import React, { Component } from "react";
import Cookies from "universal-cookie";
import "../../css/system.css";

export default class StatusChooser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      types: []
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillMount() {
    var component = this;
    const cookies = new Cookies();
    fetch("http://localhost:3000/api/types", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + cookies.get("token")
      }
    })
      .then(resp => resp.json())
      .then(function(data) {
        var systems = data;
        return systems.map(function(system) {
          /*var option = document.createElement("option");
            option.innerHTML = system.systym_name_short;
            document.getElementById('select').appendChild(option);*/
          component.setState({ types: systems });
          //console.log("state",component.state.systems);
        });
      });
  }

  handleClick(org, id, e) {
    e.preventDefault();
    //console.log(id);
    const cookies = new Cookies();
    cookies.set("type", org, { path: "/" });
    cookies.set("typeid", id, { path: "/" });
    window.location = "/mo-cabinet/send-request";
  }

  render() {
    return (
      <div className="container">
        <p className="title p-title">Выберите тип заявки</p>
        <ul className="menu-list">
          <p className="menu-label">Список доступных типов</p>
          {this.state.types.map(item => {
            return (
              <li
                onClick={e => this.handleClick(item.request, item.id, e)}
                key={item.id}
              >
                <div className="media">
                  <figure className="media-left">
                    <p className="image is-64x64">
                      <img src="/images/type.png" alt="type" />
                    </p>
                  </figure>
                  <div className="media-content">
                    <div className="content">{item.request}</div>
                  </div>
                </div>
                <hr />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
