import React, { Component } from "react";
import Cookies from "universal-cookie";
import "../../css/system.css";

export default class SystemChooser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      systems: []
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillMount() {
    var component = this;
    const cookies = new Cookies();
    fetch("http://localhost:3000/api/systems", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + cookies.get("token")
      }
    })
      .then(resp => resp.json())
      .then(function(data) {
        var systems = data;
        return systems.map(function(system) {
          /*var option = document.createElement("option");
            option.innerHTML = system.systym_name_short;
            document.getElementById('select').appendChild(option);*/
          component.setState({ systems: systems });
          //console.log("state",component.state.systems);
        });
      });
  }

  handleClick(org, id, e) {
    e.preventDefault();
    //console.log(id);
    const cookies = new Cookies();
    cookies.set("sys", org, { path: "/" });
    cookies.set("sysid", id, { path: "/" });
    window.location = "/type-chooser";
  }

  render() {
    return (
      <div className="container">
        <p className="title p-title">Выберите систему</p>
        <ul className="menu-list">
          <p className="menu-label">Список доступных систем</p>
          {this.state.systems.map(item => {
            return (
              <li
                onClick={e =>
                  this.handleClick(item.systym_name_short, item.id, e)
                }
                key={item.id}
              >
                <div className="media">
                  <figure className="media-left">
                    <p className="image is-64x64">
                      <img src="/images/system.png" alt="system" />
                    </p>
                  </figure>
                  <div className="media-content">
                    <div className="content">{item.system_name}</div>
                  </div>
                </div>
                <hr />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
