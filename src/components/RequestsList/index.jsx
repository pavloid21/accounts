import React, { Component } from "react";
import Cookies from "universal-cookie";
import DetailsPopup from "../Popup";
import Pagination from "react-js-pagination";
//import './css/requests.css';

class RequestsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalState: false,
      id: "",
      type:"",
      activePage: 1,
      requests: [],
      renderedRequests: []
    };
    this.togglePopup = this.togglePopup.bind(this);
  }

  componentWillMount() {
    var component = this;
    const cookies = new Cookies();
    fetch("http://localhost:3000/api/requests", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + cookies.get("token")
      }
    })
      .then(resp => resp.json())
      .then(function(data) {
        var requests = data;
        return requests.map(function(system) {
          component.setState({ requests: requests });
          component.setState({
            renderedRequests: requests.slice(0, 3),
            total: requests.length
          });
          console.log("state", component.state.requests);
        });
      });
  }
  togglePopup(id, type) {
    this.setState((prev, props) => {
      const newState = !prev.modalState;
      console.log("newState", newState);
      return {
        modalState: newState,
        id: id,
        type: type
      };
    });
    this.componentWillMount();
  }
  handlePageChange(pageNumber) {
    const renderedRequests = this.state.requests.slice(
      (pageNumber - 1) * 3,
      (pageNumber - 1) * 3 + 3
    );
    this.setState({
      activePage: pageNumber,
      renderedRequests: renderedRequests
    });
  }

  render() {
    return (
      <div className="container">
        <h3 className="title">Ваши заявки</h3>
        <ul>
          {this.state.renderedRequests.map(item => {
            var date = new Date(item.request_date);
            return (
              <li className="media" key={item.id}>
                <div className="media-left">
                  <figure className="image is-64x64">
                    <img src="/images/notepad.png" alt="List" />
                  </figure>
                </div>
                <div
                  className="media-content"
                  style={{ textAlign: "left", lineHeight: "normal" }}
                >
                  <p className="title is-6 notification">
                    #{item.id} {item.organisation_name}
                  </p>
                  <table className="table">
                    <thead>
                      <tr>
                        <th>{item.request}</th>
                        {(() => {
                          switch (item.request_status) {
                            case "В работе":
                              return (
                                <th className="is-info">
                                  {item.request_status}
                                </th>
                              );
                            case "Выполнено":
                              return (
                                <th className="is-primary">
                                  {item.request_status}
                                </th>
                              );
                            case "Выполнено частично":
                              return (
                                <th className="is-warning">
                                  {item.request_status}
                                </th>
                              );
                            default:
                              return;
                          }
                        })()}
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{item.system_name}</td>
                        <td>
                          <div className="content">
                            {" "}
                            {(() => {
                              if (item.request_scan != "") {
                                return (
                                  <a
                                    href={item.request_scan}
                                    onClick={() => {
                                      window.location = item.request_scan;
                                    }}
                                  >
                                    Скачать скан
                                  </a>
                                );
                              }
                            })()}
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <p className="subtitle is-6">
                    <small>
                      {date.getFullYear()}-{date.getMonth() + 1}-{date.getDate()}
                    </small>
                  </p>
                </div>
                <div className="media-right">
                  <button
                    id={item.id}
                    typereq={item.request}
                    className="button is-primary"
                    onClick={e => {
                      this.togglePopup(e.target.id, e.target.getAttribute('typereq'));
                    }}
                  >
                    Подробнее
                  </button>
                  {this.state.modalState ? (
                    <DetailsPopup
                      closeModal={this.togglePopup.bind(this)}
                      modalState={this.state.modalState}
                      typereq={this.state.type}
                      title={this.state.id}
                    />
                  ) : null}
                </div>
              </li>
            );
          })}
        </ul>
        <Pagination
          activePage={this.state.activePage}
          pageRangeDisplayed={5}
          itemsCountPerPage={3}
          totalItemsCount={this.state.total}
          innerClass="pagination-list"
          linkClass="pagination-link"
          activeLinkClass="pagination-link is-current"
          disabledClass="disabled"
          onChange={this.handlePageChange.bind(this)}
        />
      </div>
    );
  }
}
export default RequestsList;
