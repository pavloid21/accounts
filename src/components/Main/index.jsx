import React, { Component } from "react";

class Main extends Component {
  render() {
    return (
      <div className="Main">
        <section className="hero is-primary is-fullheight">
          <div className="hero-body">
            <div className="hero-body-bg" />
            <div className="container has-text-centered">
              <h1 className="title">
                Управление учетными записями систем РФ ЕГИС З
              </h1>
              <h2 className="subtitle">Запрос | Выдача | Аннулирование</h2>
              <div className="container has-text-centered">
                <a className="button is-info" href="/login">
                  Вход для МО
                </a>
                <span> </span>
                <a className="button is-primary" href="/login-vendor">Вход для вендора</a>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default Main;
