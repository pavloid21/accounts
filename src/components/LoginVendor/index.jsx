import React, { Component } from "react";
import Cookies from "universal-cookie";
import ReactDOM from "react-dom";
import VendorCabinet from "../VendorCabinet";
import "../Login/css/Login.css";
import Nav from "../Nav";

var axios = require("axios");

class LoginVendor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValues: {}
    };
  }

  handleChange(event) {
    event.preventDefault();
    let formValues = this.state.formValues;
    let name = event.target.name;
    let value = event.target.value;
    formValues[name] = value;
    this.setState({ formValues });
  }

  onSave(event) {
    event.preventDefault();
    const cookies = new Cookies();
    axios
      .post("http://localhost:3000/api/users/login", this.state.formValues, {
        headers: {
          "Access-Control-Allow-Origin": "*"
        }
      })
      .then(response => {
        cookies.set("token", response.data.id_token, { path: "/" });
        //window.sessionStorage.setItem("key", response.data.token);
      });
    axios
      .get(
        "http://localhost:3000/api/users/check/" + this.state.formValues.login,
        {
          headers: {
            "Access-Control-Allow-Origin": "*"
          }
        }
      )
      .then(response => {
        cookies.set("id", response.data.id, { path: "/" });
        let root = document.getElementById("root");
        root.className = "new";
        ReactDOM.render(
          <div>
            <Nav />
            <VendorCabinet token={cookies.get("token")} />
          </div>,
          document.getElementById("root")
        );
      });
  }

  render() {
    return (
      <div className="hero is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-4 is-offset-4">
              <div className="form">
                <h3 className="title has-text-grey">Авторизация</h3>
                <p className="subtitle has-text-grey">
                  Войдите, чтобы продолжить работу
                </p>
                <div className="box">
                  <figure className="avatar">
                    <img src="/images/businessman.png" alt="avatar" />
                  </figure>

                  <form onSubmit={this.onSave.bind(this)}>
                    <div className="field">
                      <div className="control">
                        <input
                          type="text"
                          className="input is-large"
                          name="login"
                          value={this.state.formValues["login"]}
                          onChange={this.handleChange.bind(this)}
                        />
                      </div>
                    </div>
                    <div className="field">
                      <div className="control">
                        <input
                          type="password"
                          className="input is-large"
                          name="password"
                          value={this.state.formValues["password"]}
                          onChange={this.handleChange.bind(this)}
                        />
                      </div>
                    </div>
                    <div className="field">
                      <label className="checkbox">
                        <input type="checkbox" />
                        Запомнить меня
                      </label>
                    </div>
                    <button
                      type="submit"
                      className="button is-block is-info is-large"
                      style={{ width: 100 + "%" }}
                    >
                      Войти
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginVendor;
