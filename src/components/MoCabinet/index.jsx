import React, { Component } from "react";

class MoCabinet extends Component {
  constructor(props) {
    super();
  }
  render() {
    return (
      <div className="container" style={{ width: 70 + "%" }}>
        <p className="title">Личный кабинет</p>
        <br />
        <div className="level">
          <div className="level-left" style={{ width: 40 + "%" }}>
            <div className="card">
              <div className="card-image">
                <figure className="image is-4by3">
                  <img src="../images/question.jpg" alt="Отправить заявку" />
                </figure>
              </div>
              <div className="card-content">
                <div className="content">
                  <p className="title is-4">Отправить заявку</p>
                  <p className="subtitle is-6">
                    Заполнить заявку на получение, блокировку или расширение
                    учетной записи в информационных системах РФ ЕГИС З
                  </p>
                  <a className="button is-info" href="/sys-chooser">
                    Перейти к заполнению
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="level-right" style={{ width: 40 + "%" }}>
            <div className="card">
              <div className="card-image">
                <figure className="image is-4by3">
                  <img src="../images/checklist.jpg" alt="Отправить заявку" />
                </figure>
              </div>
              <div className="card-content">
                <div className="content">
                  <p className="title is-4">Мои заявки</p>
                  <p className="subtitle is-6">
                    Проверить статус исполнения заявки на присвоение учетной
                    записи в системах РФ ЕГИСЗ.
                  </p>
                  <a className="button is-info" href="/requests-list">
                    Перейти к просмотру
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MoCabinet;
