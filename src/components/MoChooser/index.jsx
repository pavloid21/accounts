import React, { Component } from "react";
import Cookies from "universal-cookie";
import "../../css/chooser.css";
import { RiseLoader } from "halogenium";

var axios = require("axios");
export default class MoChooser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      mo: []
    };
    this.loadData = this.loadData.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  loadData() {
    const cookies = new Cookies();
    var component = this;
    this.setState(
      {
        loading: true
      },
      () => {
        console.log("id", cookies.get("id"));
        while (cookies.get("id") == null) {}
        axios
          .post(
            "http://localhost:3000/api/users-mo",
            { id: cookies.get("id") },
            {
              headers: {
                "Access-Control-Allow-Origin": "*",
                Authorization: "Bearer " + cookies.get("token")
              }
            }
          )
          .then(function(data) {
            console.log("data", data.data);
            var mo = data.data;
            return mo.map(function(item) {
              component.setState({
                loading: false,
                mo: mo
              });
            });
            console.log("state", component.state.mo);
          })
          .catch(err => {
            component.setState({ mo: "error" });
          });
      }
    );
  }

  componentWillMount(props) {
    this.loadData();
  }

  handleClick(org, id_org, e) {
    e.preventDefault();
    //console.log(id);
    const cookies = new Cookies();
    cookies.set("mo", org, { path: "/" });
    cookies.set("id_org", id_org, { path: "/" });
    window.location = "/mo-cabinet";
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="hero is-fullheight">
          <div className="hero-body">
            <div className="container has-text-centered">
              <RiseLoader color="#00d1b2" />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="container">
          <h3 className="title is-3">Выберите организацию</h3>
          <ul className="menu-list">
            <p className="menu-label">Список доступных организаций</p>
            {this.state.mo.map(item => {
              return (
                <li
                  onClick={e =>
                    this.handleClick(
                      item.organisation_name,
                      item.id_organisation,
                      e
                    )
                  }
                  key={item.id_organisation}
                >
                  <div className="media">
                    <figure className="media-left">
                      <p className="image is-64x64">
                        <img src="/images/hospital.png" alt="hospital" />
                      </p>
                    </figure>
                    <div className="media-content">
                      <div className="content">{item.organisation_name}</div>
                    </div>
                  </div>
                  <hr />
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
  }
}
