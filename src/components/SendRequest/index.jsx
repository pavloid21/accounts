import React, { Component } from "react";
import Docxtemplater from "docxtemplater";
import template from "./template/template.docx";

class SendRequest extends Component {
  constructor() {
    super();
    this.uploadFile = this.uploadFile.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.addUser = this.addUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.loadFile = this.loadFile.bind(this);
    this.state = {
      id: "0",
      name: "",
      sname: "",
      fname: "",
      snils: "",
      mo: "",
      system: "МИС",
      position: ""
    };
    this.users = { user: [] };
    this.count = 1;
  }
  loadFile(url, callback) {
    console.log("lo", url);
    window.JSZipUtils.getBinaryContent(url, callback.bind(this));
  }

  uploadFile(event) {
    let file = event.target.files[0];
    console.log(file.name);
    document.getElementById("file").innerHTML = file.name;
    if (file) {
      let data = new FormData();
      data.append("file", file);
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
    console.log(event.target.name + ":" + event.target.value);
    event.preventDefault();
  }

  addUser(event) {
    event.preventDefault();
    this.setState({ id: this.count });
    this.users["user"].push(this.state);
    this.count++;
  }

  removeItem(event) {
    event.preventDefault();
    //alert("removed");
    alert(this.users["user"]);
  }

  render() {
    let rows = this.users.user.map(person => {
      return <PersonRow key={person.id} data={person} />;
    });
    /*  this.loadFile(template, function(error, content){
      console.log('run!', template);
      if (error) {throw error};
      var zip = new window.JSZip(content);
      var doc = new Docxtemplater().loadZip(zip);
      doc.setData(this.users);

    try {
      doc.render()
    }
    catch (error) {
      var e = {
        message: error.message,
        name: error.name,
        stack: error.stack,
        properties: error.properties
      }
      console.log(JSON.stringify({error:e}));
      throw error;
    }
    var out = doc.getZip().generate({
      type:"blob",
      mimType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    })
    window.saveAs(out, "output.docx")
  });*/
    return (
      <div className="container" style={{ width: 80 + "%" }}>
        <h1>
          <strong>Заявка на учетную запись в ИС</strong>
        </h1>
        <br />
        <div className="level">
          <div className="level-item">
            <div className="container" style={{ width: 70 + "%" }}>
              <div className="field">
                <label className="label">Информационная система</label>
                <div className="control">
                  <div className="select">
                    <select
                      name="system"
                      onChange={this.handleChange}
                      value={this.state.system}
                    >
                      <option>МИС</option>
                      <option>Web-Своды</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="field">
                <label className="label">Имя</label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    name="name"
                    onChange={this.handleChange}
                    value={this.state.name}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Фамилия</label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    name="sname"
                    onChange={this.handleChange}
                    value={this.state.sname}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Отчество</label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    name="fname"
                    onChange={this.handleChange}
                    value={this.state.fname}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">СНИЛС</label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    name="snils"
                    onChange={this.handleChange}
                    value={this.state.snils}
                  />
                </div>
                <p className="help">
                  Вводить без пробелов и дополнительных символов разделения
                </p>
              </div>
              <div className="field">
                <label className="label">Должность</label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    name="position"
                    onChange={this.handleChange}
                    value={this.state.position}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Медицинская организация</label>
                <div className="control">
                  <input
                    className="input"
                    type="text"
                    placeholder="Здесь будет автоматически имя МО после регистрации"
                    name="mo"
                    onChange={this.handleChange}
                    value={this.state.mo}
                  />
                </div>
              </div>
              <div className="fied">
                <button
                  className="button is-primary is-outlined"
                  onClick={this.addUser}
                >
                  <span className="icon">
                    <i className="fa fa-plus-square" />
                  </span>
                  <span>Добавить пользователя</span>
                </button>
              </div>
            </div>
          </div>
          <br />
          <div className="level-item">
            <table className="table is-hoverable">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Фамилия</th>
                  <th>Имя</th>
                  <th>Отчество</th>
                  <th>СНИЛС</th>
                  <th>Должность</th>
                  <th>ИС</th>
                  <th>МО</th>
                  <th />
                </tr>
              </thead>
              <tbody>{rows}</tbody>
            </table>
          </div>
        </div>
        <div className="field">
          <div className="file is-primary has-name">
            <label className="file-label">
              <input
                className="file-input"
                type="file"
                name="resume"
                onChange={this.uploadFile}
              />
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fa fa-upload" />
                </span>
                <span className="file-label">Загрузить …</span>
              </span>
              <span className="file-name" id="file">
                Скан заявки
              </span>
            </label>
          </div>
        </div>
        <div className="field">
          <div className="control">
            <label className="checkbox">
              <input type="checkbox" />
              Я принимаю{" "}
              <a href="/">соглашение на обработку персональных данных</a>
            </label>
          </div>
        </div>
        <button className="button is-link">Отправить</button>

        <br />
      </div>
    );
  }
}

const PersonRow = props => {
  return (
    <tr>
      <td>{props.data.id}</td>
      <td>{props.data.sname}</td>
      <td>{props.data.name}</td>
      <td>{props.data.fname}</td>
      <td>{props.data.snils}</td>
      <td>{props.data.position}</td>
      <td>{props.data.system}</td>
      <td>{props.data.mo}</td>
      <td>
        <button
          className="button is-danger is-outlined"
          onClick={() => {
            alert(JSON.stringify(props.data));
          }}
        >
          <span className="icon">
            <i className="fa fa-minus-circle" />
          </span>
        </button>
      </td>
    </tr>
  );
};
export default SendRequest;
