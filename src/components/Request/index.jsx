import React, { Component } from "react";
import Cookies from "universal-cookie";
import template from "../../template/template.docx";
import Docxtemplater from "docxtemplater";
import Axios from "axios";
import Select from 'react-select';

class Request extends Component {
  constructor(props) {
    super(props);
    this.state = {
      system: {},
      mo: {},
      products: [],
      button: false,
      role: [],
      selectedOption:null,
      option:null,
      type:{}
    };
    this.loadFile = this.loadFile.bind(this);
  }
  componentWillMount() {
    var cookies = new Cookies();
    var sysid = cookies.get("sysid");
    switch (cookies.get("typeid")) {
      case 1: this.setState({type:"завести"});
        break;
      case 2: this.setState({type:"расширить"});
        break;
      case 3: this.setState({type:"заблокировать"});
        break;
      default: this.setState({type:"завести"});

    }
    Axios.get("http://localhost:3000/api/systems/sys-name/" + sysid, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        Authorization: "Bearer " + cookies.get("token")
      }
    }).then(response => {
      console.log("RESPONSE", response.data);
      this.setState({ role: response.data });
      this.setState({ role: this.state.role[0].module_name });

      console.log("STATE", this.state.type);
      this.handleAddEvent();
    });
  }

  loadFile(url, callback) {
    console.log("lo", url);
    window.JSZipUtils.getBinaryContent(url, callback);
  }

  downloadDoc(event) {
    event.preventDefault();
    //console.log(this.state.products);
    let cookie = new Cookies();
    this.setState({ system: cookie.get("sys") });
    this.setState({ mo: cookie.get("mo") });

    console.log("STATE DOWNLOAD", this.state);
    this.loadFile(
      template,
      function(error, content) {
        console.log("run!", template);
        if (error) {
          throw error;
        }
        var zip = new window.JSZip(content);
        var doc = new Docxtemplater().loadZip(zip);

        //console.log("state", this.state.system);
        doc.setData(this.state);
        try {
          doc.render();
        } catch (error) {
          var e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties
          };
          console.log(JSON.stringify({ error: e }));
          throw error;
        }
        var out = doc.getZip().generate({
          type: "blob",
          mimeType:
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        });
        window.saveAs(out, "output.docx");
      }.bind(this)
    );
  }

  uploadFile(event) {
    let file = event.target.files[0];
    console.log(file.name);
    document.getElementById("file").innerHTML = file.name;
    if (file) {
      let data = new FormData();
      data.append("file", file);
    }
  }
  handleUserInput(filterText) {
    this.setState({ filterText: filterText });
  }
  handleRowDel(product) {
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  }
  handleSave() {
    let cookies = new Cookies();
    Axios.post(
      "http://localhost:3000/api/add-request",
      {
        org: cookies.get("id_org"),
        user: cookies.get("id"),
        sys: cookies.get("sysid"),
        id_type: cookies.get("typeid")
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + cookies.get("token")
        }
      }
    ).then(resp => {
      resp.data.map(item => {
        cookies.set("id_req", item.id);
      });
      this.state.products.map(row => {
        console.log("row", row);
        Axios.post(
          "http://localhost:3000/api/add-sys-user",
          {
            id_req: cookies.get("id_req"),
            position: row.position,
            surname: row.sname,
            name: row.name,
            father_name: row.fname,
            birth_date: row.bdate,
            id_type: cookies.get("typeid")
          },
          {
            headers: {
              "Access-Control-Allow-Origin": "*",
              Authorization: "Bearer " + cookies.get("token")
            }
          }
        ).then(response => {
          //console.log(response);
          this.state.products.map(item => {
            item.role.map(option =>{
              Axios.post("http://localhost:3000/api/insert-user-module",
            {
              requestuser:response.data.insertId,
              module:option.value
            },
            {
              headers: {
                "Access-Control-Allow-Origin": "*",
                Authorization: "Bearer " + cookies.get("token")
              }
            }
          ).then(response => {
            console.log(response);
          })
            })
          });
        });
      });

    });

  }

  handleAddEvent(evt) {
    var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var cookies = new Cookies();
    var mo = cookies.get("mo");
    var sys = cookies.get("sys");

    var roleDefault = this.state.role;
    var product = {
      id: id,
      name: "",
      sname: "",
      fname: "",
      bdate: "",
      mo: mo,
      role: roleDefault,
      system: sys,
      position: ""
    };
    this.state.products.push(product);
    this.setState(this.state.products);
  }

  checkboxChecked(e) {
    this.setState({ button: e.target.checked });
  }

  handleProductTable(evt) {
    //console.log(evt.target);
    var cookies = new Cookies();
    var mo = cookies.get("mo");

    var item = {
      id: evt.target.id,
      name: evt.target.name,
      text: evt.target.value
    };
    //console.log('target', evt.target);
    //console.log('item', item);
    var products = this.state.products.slice();
    var newProducts = products.map(function(product) {
      for (var key in product) {
        if (key === item.name && product.id === item.id) {
          product[key] = item.text;
          product["mo"] = mo;
        }
      }
      return product;
    });
    this.setState({ products: newProducts });
    console.log("products", this.state.products);
  }

  handleSelectChanged(selectedOption) {
    var products = this.state.products.slice();
    var newProducts = products.map(function(product) {
      for (var key in product) {
        if (selectedOption.length == 0) {
          //product["role"] = [];
          //console.log();
        } else {
        if (product.id === selectedOption[0].id) {

          product["role"] = selectedOption;
        }
      }
      }
      return product;
    });
    this.setState({ products: newProducts });
    console.log(`Option selected:`, this.state.products);
    //alert(e.target.options);
  }

  render() {
    var cookies = new Cookies();
    var mo = cookies.get("mo");
    return (
      <div className="container">
        <h3 className="title">Добавление пользователей</h3>
        <br />
        <div className="content">{mo}</div>
        <ProductTable
          onSelectChanged={this.handleSelectChanged.bind(this)}
          onProductTableUpdate={this.handleProductTable.bind(this)}
          onRowAdd={this.handleAddEvent.bind(this)}
          onRowDel={this.handleRowDel.bind(this)}
          products={this.state.products}
        />
        <div className="field">
          <button
            className="button is-link"
            onClick={this.downloadDoc.bind(this)}
          >
            Скачать бланк заявки
          </button>
        </div>

        <div className="field">
          <div className="control">
            <label className="checkbox">
              <input
                id="checkbox"
                type="checkbox"
                onChange={this.checkboxChecked.bind(this)}
              />
              Я принимаю{" "}
              <a href="/">соглашение на обработку персональных данных</a>
            </label>
          </div>
        </div>
        {this.state.button ? (
          <button
            className="button is-link"
            onClick={this.handleSave.bind(this)}
          >
            Сохранить
          </button>
        ) : (
          <button className="button is-link" disabled>
            Сохранить
          </button>
        )}
      </div>
    );
  }
}

class ProductTable extends React.Component {
  render() {
    var onProductTableUpdate = this.props.onProductTableUpdate;
    var onSelectChanged = this.props.onSelectChanged;
    var rowDel = this.props.onRowDel;
    var product = this.props.products.map(function(product) {
      return (
        <ProductRow
          onProductTableUpdate={onProductTableUpdate}
          onSelectChanged={onSelectChanged}
          product={product}
          onDelEvent={rowDel.bind(this)}
          key={product.id}
        />
      );
    });
    return (
      <div>
        <button
          type="button"
          onClick={this.props.onRowAdd}
          className="button is-primary"
        >
          Добавить
        </button>
        <table className="table is-hoverable">
          <thead>
            <tr>
              <th>Фамилия</th>
              <th>Имя</th>
              <th>Отчество</th>
              <th>Дата рождения</th>
              <th>Должность</th>
              <th>Роль</th>
              <th>ИС</th>
            </tr>
          </thead>

          <tbody>{product}</tbody>
        </table>
      </div>
    );
  }
}

class ProductRow extends React.Component {
  onDelEvent() {
    this.props.onDelEvent(this.props.product);
  }
  render() {
    var cookies = new Cookies();
    return (
      <tr className="eachRow">
        <EditableCell
          onProductTableUpdate={this.props.onProductTableUpdate}
          cellData={{
            type: "sname",
            value: this.props.product.sname,
            id: this.props.product.id
          }}
        />
        <EditableCell
          onProductTableUpdate={this.props.onProductTableUpdate}
          cellData={{
            type: "name",
            value: this.props.product.name,
            id: this.props.product.id
          }}
        />
        <EditableCell
          onProductTableUpdate={this.props.onProductTableUpdate}
          cellData={{
            type: "fname",
            value: this.props.product.fname,
            id: this.props.product.id
          }}
        />
        <DatePicker
          onProductTableUpdate={this.props.onProductTableUpdate}
          cellData={{
            type: "bdate",
            value: this.props.product.bdate,
            id: this.props.product.id
          }}
        />
        <EditableCell
          onProductTableUpdate={this.props.onProductTableUpdate}
          cellData={{
            type: "position",
            value: this.props.product.position,
            id: this.props.product.id
          }}
        />
        <EditableSysDropdown
          name="roles"
          roleid={cookies.get("sysid")}
          onSelectChanged={this.props.onSelectChanged}
          cellData={{
            type: "role",
            value: this.props.product,
            id: this.props.product.id
          }}
        />
        <EditableSysDropdown name="system" />
        <td className="del-cell">
          <button
            className="delete is-medium"
            onClick={this.onDelEvent.bind(this)}
          />
        </td>
      </tr>
    );
  }
}
class EditableCell extends React.Component {
  render() {
    return (
      <td>
        <input
          className="input"
          type="text"
          name={this.props.cellData.type}
          id={this.props.cellData.id}
          value={this.props.cellData.value}
          onChange={this.props.onProductTableUpdate}
        />
      </td>
    );
  }
}

class DatePicker extends React.Component {
  render() {
    return (
      <td>
        <input
          className="input"
          type="date"
          name={this.props.cellData.type}
          id={this.props.cellData.id}
          value={this.props.cellData.value}
          onChange={this.props.onProductTableUpdate}
        />
      </td>
    );
  }
}

//System & Roles list
class EditableSysDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
      role: [],
      position: [],
      system: ""
    };
  }
  componentWillMount() {
    const cookies = new Cookies();
    Axios.get(
      "http://localhost:3000/api/systems/sys-name/" + this.props.roleid,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
          Authorization: "Bearer " + cookies.get("token")
        }
      }
    ).then(response => {
      console.log("props", this.props.roleid);
      this.setState({ role: response.data });
      console.log("response", response.data);
    });
    this.setState({ system: cookies.get("sys") });

    //this.setState({options:this.state.options});

  }

  handleChange = (selectedOption) => {
    this.setState({selectedOption});
    console.log(`Option selected:`, selectedOption);
  }

  render() {
    const option = this.state.role.map((item)=>{
      return {label:item.module_name, value:item.id, id:this.props.cellData.id}
    })
    //console.log(option);
    if (this.props.name === "system") {
      return (
        <td>
          <div>{this.state.system}</div>
        </td>
      );
    }
    return (
      <td style={{width:'170px'}}>
        <Select options={option} onChange={this.props.onSelectChanged} placeholder="Выбрать..." isMulti isSearchable></Select>
      </td>
    );
  }
}

export default Request;
