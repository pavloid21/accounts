import React, { Component } from "react";
import Axios from "axios";
import Cookies from "universal-cookie";
import "../../css/popup.css";

class DetailsPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],
      filename: ""
    };
  }

  uploadFile(event) {
    let file = event.target.files[0];
    console.log(file.name);
    if (file) {
      var cookies = new Cookies();
      this.setState({ filename: file.name });
      let data = new FormData();
      data.append("file", file);
      data.append("filename", file.name);
      let s = cookies.get("mo").replace('"', "");
      s = s.replace('"', "");
      data.append("org", s);
      data.append("sys", cookies.get("sys"));
      data.append("type", this.props.typereq);
      data.append("reqid", this.props.title);
      console.log("TitltTT", cookies.get("type"));
      Axios.post("http://localhost:3000/api/upload", data, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          Authorization: "Bearer " + cookies.get("token")
        }
      }).then(function(response) {
        this.setState({ filename: "Загружено!" });
      });
      this.setState({ filename: "Загружено!" });
    }
  }

  componentWillMount() {
    let cookies = new Cookies();
    let id = this.props.title;
    console.log("title", this.props.typereq);
    Axios.get("http://localhost:3000/api/request_details/" + id, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        Authorization: "Bearer " + cookies.get("token")
      }
    }).then(response => {
      var details = response.data;
      return details.map(item => {
        this.setState({ details: details });
      });
    });
  }

  render() {
    console.log(this.props.modalState);
    if (!this.props.modalState) {
      return null;
    }
    return (
      <div className="modal is-active">
        <div className="modal-background" onClick={this.props.closeModal} />
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">
              Список из заявки # {this.props.title}
            </p>
            <button className="delete" onClick={this.props.closeModal} />
          </header>
          <section className="modal-card-body">
            <div className="content is-size-7">
              <table className="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Должность</th>
                    <th>Дата рождения</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th>Логин</th>
                    <th>Пароль</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.details.map(item => {
                    var date = new Date(item.birth_date);
                    return (
                      <tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.position}</td>
                        <td>
                          {date.getFullYear()}-{date.getMonth() + 1}-{date.getDate()}
                        </td>
                        <td>{item.surname}</td>
                        <td>{item.name}</td>
                        <td>{item.father_name}</td>
                        <td>{item.login}</td>
                        <td>{item.password}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </section>
          <footer className="modal-card-foot">
            <a className="button" onClick={this.props.closeModal}>
              Закрыть
            </a>
            <div
              className="file has-name is-info"
              onChange={this.uploadFile.bind(this)}
            >
              <label className="file-label">
                <input type="file" className="file-input" name="resume" />
                <span className="file-cta">
                  <span className="file-icon">
                    <i className="fas fa-upload" />
                  </span>
                  <span className="file-label">Скан заявки ...</span>
                </span>
                <span id="file-name" className="file-name">
                  {this.state.filename}
                </span>
              </label>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default DetailsPopup;
