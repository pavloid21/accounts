import React, { Component } from "react";
import "../../css/shadow.css";

class Nav extends Component {
  render() {
    return (
      <nav className="navbar nav is-primary one-edge-shadow">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item" href="/">
              <div className="logo" />
              ГУЗ "МИАЦ"
            </a>
            <span className="navbar-burger burger" data-target="navMenu">
              <span />
              <span />
              <span />
            </span>
          </div>
          <div id="navMenu" className="navbar-menu">
            <div className="navbar-end">
              <a className="navbar-item" href="/">
                Главная
              </a>
              <a className="navbar-item" href="/login">
                Личный кабинет
              </a>
              <a className="navbar-item" href="/info">
                О системе
              </a>
              <a className="navbar-item" href="/docs">
                Документы
              </a>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}
export default Nav;
